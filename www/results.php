<?php
session_start();

$normal = 0;
$redGreen = 0;
$prota = 0;
$deuter = 0;
$noIndication = 0;
$resultsHTML;

foreach ($_SESSION["orderedPlates"] as $plate) {

	if ($plate["indication"] == "Normal") {
		$normal++;
	} else if ($plate["indication"] ==  "Red-Green") {
		$redGreen++;
	} else if ($plate["indication"] ==  "Protanopia or Protanomaly") {
		$prota++;
	} else if ($plate["indication"] ==  "Deuteranopia or Deuteranomaly") {
		$deuter++;
	} else if ($plate["indication"] ==  "No Indication") {
		$noIndication++;
	}

	$resultsHTML = $resultsHTML . "<table>
									<tr>
										<td>Plate ID:</td>
										<td>". $plate["plate-name"] ."</td>
									</tr>
									<tr>
										<td>User Answer:</td>
										<td>". $plate["user-answer"] ."</td>
									</tr>
									<tr>
										<td>Correct Answer Was:</td>
										<td>". $plate["answer"] ."</td>
									</tr>
									<tr>
										<td>User Answer Indicates:</td>
										<td>". $plate["indication"] ."</td>
									</tr>
									</table>
									
									<hr>";

}



?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Results Page</title>
	</head>
	<body>
		<a href="restart.php">Restart</a>
	 	
	 	<h3>Your test results indicate...</h3>	

	 		<?php if($normal >= 22) { ?>
	 	<h3>You appear to have normal vision!</h3>
	 		<?php } else if($normal <= 15 && ($prota > $deuter)) { ?>
	 	<h3>You appear to have protanopia or protanomaly.</h3>
	 		<?php } else if($normal <= 15 && ($prota < $deuter)) {?>
	 	<h3>You appear to have deuteranopia or deuteranomaly.</h3>
	 		<?php } else { ?>
	 	<h3>Your test did not provide accurate results. Further testing is necessary.</h3>
	 		<?php } ?>

	 	<h1>Results Overview</h1>
			<table>
				<tr>
					<td><strong>Vision Type</strong></td>
					<td><strong>Tally</strong></td>
				</tr>
				<tr>
					<td>Normal Vision</td>
					<td><?=$normal?></td>
				</tr>
				<tr>
					<td>Red Green Deficiency</td>
					<td><?=$redGreen?></td>
				</tr>
				<tr>
					<td>Protanopia or Protanomaly</td>
					<td><?=$prota?></td>
				</tr>
				<tr>
					<td>Deuteranopia or Deuteranomaly</td>
					<td><?=$deuter?></td>
				</tr>
				<tr>
					<td>No Indication</td>
					<td><?=$noIndication?></td>
				</tr>	
			</table> 

		<h1>Results By Plate:</h1>
		<?php echo $resultsHTML; ?>
	</body>
</html>