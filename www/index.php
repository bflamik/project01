<?php
session_start();

function randomize_plate($list) {
	foreach($list as $id => $value) {
		$randomized_plate[] = $id;
	}	
	shuffle($randomized_plate);
	return $randomized_plate;
}

function checkAnswer($value, $key) {
	foreach ($key as $answer) {
		if ($value == $answer) {
			return true;
		}
	}
	return false;
}

if ($_SESSION["questionNumber"] == 24) {
	header('Location: results.php');
}

if ($_SESSION["questionNumber"] == 25) {
	header('Location: restart.php');
}


if (isset($_GET["oldQuestionNumber"])) {
	if ($_GET["oldQuestionNumber"] == $_SESSION["questionNumber"]) {
		$_SESSION["questionNumber"]++;

		$plateName = $_SESSION["randomPlates"][$_GET["oldQuestioNumber"]];

		$_SESSION["orderedPlates"][$plateName]["user-answer"] = $_GET["answer"];
		$_SESSION["orderedPlates"][$plateName]["plate-name"] = $plateName;


		if ($_SESSION["orderedPlates"][$plateName]["answer"] == $_GET["answer"]) {
			$_SESSION["orderedPlates"][$plateName]["indication"] = "Normal";
		}

		if (isset($_SESSION["orderedPlates"][$plateName]["indicates"]["red-green"])) {
			if (checkAnswer($_GET["answer"], $_SESSION["orderedPlates"][$plateName]["indicates"]["red-green"]) {
				$_SESSION["orderedPlates"][$plateName]["indication"] = "Red-Green";
			}
		}

		if (isset($_SESSION["orderedPlates"][$plateName]["indicates"]["protanopia or protanomaly"])) {
			if (checkAnswer($_GET["answer"], $_SESSION["orderedPlates"][$plateName]["indicates"]["protanopia or protanomaly"])) {
				$_SESSION["orderedPlates"][$plateName]["indication"] = "Protanopia or Protanomaly";
			}
		}

		if (isset($_SESSION["orderedPlates"][$plateName]["indicates"]["deuteranopia or deuteranomaly"])) {
			if (checkAnswer($_GET["answer"], $_SESSION["orderedPlates"][$plateName]["indicates"]["deuteranopia or deuteranomaly"])) {
				$_SESSION["orderedPlates"][$plateName]["indication"] = "Deuteranopia or Deuteranomaly";
			}
		}

		if (!isset($_SESSION["orderedPlates"][$plateName]["indication"])) {
			$_SESSION["orderedPlates"][$plateName]["indication"] = "No Indication";
		}
	}
}

if (!isset($_SESSION["orderedPlates"])) {
	$_SESSION["orderedPlates"] = json_decode(file_get_contents("answer_key.json"), true);
	$_SESSION["randomPlates"] = randomize_plate($_SESSION["orderedPlates"]);
	$_SESSION["questionNumber"] = 0;
}


?>
<!DOCTYPE html>
<html>
	<head>
		<title>Project 1</title>
	</head>
	<body>

		<a href="restart.php">restart</a>
		<h1>Welcome to the Colorblindness Test</h1>
		<h2>viewing <?=$_SESSION["randomPlates"][$_SESSION["questionNumber"]]?></h2>
		<h3>question number <?=$_SESSION["questionNumber"]+1?> out of 25</h3>

		
		<img src="test-images/<?=$_SESSION["randomPlates"][$_SESSION["questionNumber"]]?>.png" style="width:237px; height:234px;"/>

		<form method="GET" action="index.php">
	  		<input type="text" name="answer" autofocus>
	  		<!--Invisible field:--><input type="text" style="display: none;" name="oldQuestionNumber" value="<?=$_SESSION["questionNumber"]?>" readonly>
	  		<button type="submit" value="Submit">continue-></button>
		</form>
	</body>
</html>